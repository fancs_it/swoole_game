<?php

namespace app\common\lib;

class Redis
{
    /**
     * 短信key前缀
     */
    protected static $smsPre = 'sms_';
    /**
     * 用户key前缀
     */
    protected static $userPre = 'user_';

    public static function smsKey($phone)
    {
        return self::$smsPre . $phone;   
    }

    public static function userKey($phone)
    {
        return self::$userPre . $phone;   
    }
}
