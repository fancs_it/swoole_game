<?php


namespace app\common\lib;

class Util 
{
    public static function show($status,$message='',$data=[]) 
    {
        $data = [
            'status'    =>  $status,
            'message'   =>  $message,
            'data'      =>  $data
        ];

        return json($data);
    }
}