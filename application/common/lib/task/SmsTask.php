<?php


namespace app\common\lib\task;
use think\swoole\facade\Task;
use app\common\lib\Sms;
use app\common\lib\Predis;
use app\common\lib\Redis as RedisLib;
class SmsTask
{
    /**
     * 发送短信任务
     */
    public static function send($mobile,$code)
    {
        Task::async(function ($serv, $task_id, $data) use($mobile,$code){
            
            try {
                $response = Sms::sendSms($mobile,$code);
            } catch (\Throwable $th) {
                //throw $th;
                return FALSE;
            }
            if($response->Code === 'OK'){
                //携程redis
                // $redis = new \Swoole\Coroutine\Redis();
                // $redis->connect(config('redis.host'), config('redis.port'));
                // $redis->set(RedisLib::smsKey($mobile),$code,config('redis.out_time'));

                Predis::getInstance()->set(RedisLib::smsKey($mobile),config('redis.out_time'),$code);
            }
       
            return TRUE;
        });
    }
}