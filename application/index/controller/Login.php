<?php


namespace app\index\controller;

use app\common\lib\Util;
use app\common\lib\Predis;
use app\common\lib\Redis as RedisLib;
use think\Controller;

class Login extends Controller
{
    public function index()
    {
        if(request()->isPost()){
            $mobile = $this->request->param('phone_num','');
            $code = $this->request->param('code',0);
            
            if(empty($mobile) || !$code){
               return Util::show(config('code.error'),'Phone number or code is error');
            }
            
            $redisCode = Predis::getInstance()->get(RedisLib::smsKey($mobile));
            
            if($redisCode !== $code) return Util::show(config('code.error'),'Code error');

            $data = [
                'user'      =>  $mobile,
                'userKey'   =>  md5(RedisLib::userKey($mobile)),
                'time'      =>  time(),
                'isLogin'   =>  true 
            ];
            Predis::getInstance()->set(RedisLib::userKey($mobile),$data);

            cookie('user',$data,24*3600);

            return Util::show(config('code.success'),'ok',$data);
        }
    } 
}