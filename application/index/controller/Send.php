<?php


namespace app\index\controller;

use app\common\lib\Util;
use app\common\lib\task\SmsTask;
use app\common\lib\Redis as RedisLib;


class Send 
{
    /**
     * 发送短信
     */
    public function index()
    {
        $mobile = request()->param('phone_num','');

        if(empty($mobile)){
            return Util::show(config('code.error'),'手机号不能为空');
        }

        $code = rand(100000,999999);
        //通过swoole task任务投递短信发送任务
        SmsTask::send($mobile,$code);

        return Util::show(config('code.success'),'ok');
    } 
}